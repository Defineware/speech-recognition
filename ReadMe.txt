Overview:-
Based on the samples supplied by Microsoft with developing Speech Recongnition systems.
This code has been converted to be used as an object. 
Will listen using the default SR engine. If a word is recongised it will return what is recieved as a string. 

Requirements:-
This project has been built via Visual Studio Community 2015
Project needs to be run only in Release mode as SDK will throw build errors
Can be built in either x86 or x64 framework

Dependencies:-
Microsoft Speeck SDK 5.1 to be installed

Usage:
#include "listen.h" // in file that will call the below
Listen listen;//listen object
	
//continue to listen
do {
	CSpDynamicString str = "";
	str = listen.hearing();
	// action to be carried out using str. eg: pass string to database query
} while (listen.getheard() <= 0);

Use as own risk as I do not guarantee any code provided thoughout