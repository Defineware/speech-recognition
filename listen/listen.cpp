#include "listen.h"


inline HRESULT Listen::BlockForResult(ISpRecoContext * pRecoCtxt, ISpRecoResult ** ppResult)
{
	HRESULT hr = S_OK;
	CSpEvent event;

	while (SUCCEEDED(hr) &&
		SUCCEEDED(hr = event.GetFrom(pRecoCtxt)) &&
		hr == S_FALSE)
	{
		printf("trying to find something\r\n");
		hr = pRecoCtxt->WaitForNotifyEvent(INFINITE);//INFINITE
	}
	printf("found something!!\r\n");
	*ppResult = event.RecoResult();
	if (*ppResult)
	{
		(*ppResult)->AddRef();
	}

	return hr;
}

//not used as commands such as "start" will open the start menu. Use instead Listen::hearing()
CSpDynamicString Listen::hearing_shared()
{
	CComPtr<ISpRecoContext> cpRecoCtxt;
	CComPtr<ISpRecoGrammar> cpGrammar;
	CComPtr<ISpVoice> cpVoice;
	//uses the windows commands queries when talking and loads up the Speech Reco software, words such as "start"
	// by itself will click on the start menu
	hr = cpRecoCtxt.CoCreateInstance(CLSID_SpSharedRecoContext);

	if (SUCCEEDED(hr))
	{
		hr = cpRecoCtxt->GetVoice(&cpVoice);
	}

	if (cpRecoCtxt && cpVoice &&
		SUCCEEDED(hr = cpRecoCtxt->SetNotifyWin32Event()) &&
		SUCCEEDED(hr = cpRecoCtxt->SetInterest(SPFEI(SPEI_RECOGNITION), SPFEI(SPEI_RECOGNITION))) &&
		SUCCEEDED(hr = cpRecoCtxt->SetAudioOptions(SPAO_RETAIN_AUDIO, NULL, NULL)) &&
		SUCCEEDED(hr = cpRecoCtxt->CreateGrammar(0, &cpGrammar)) &&
		SUCCEEDED(hr = cpGrammar->LoadDictation(NULL, SPLO_STATIC)) &&
		SUCCEEDED(hr = cpGrammar->SetDictationState(SPRS_ACTIVE)))
	{
		USES_CONVERSION;

		CComPtr<ISpRecoResult> cpResult;

		while (SUCCEEDED(hr = BlockForResult(cpRecoCtxt, &cpResult)))
		{
			cpGrammar->SetDictationState(SPRS_INACTIVE);

			CSpDynamicString dstrText;

			if (SUCCEEDED(cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE,
				TRUE, &dstrText, NULL)))
			{
				if (debug)
					printf("I heard:  %s\n", W2A(dstrText));

				if (fUseTTS)
				{
					cpVoice->Speak(L"I heard", SPF_ASYNC, NULL);
					cpVoice->Speak(dstrText, SPF_ASYNC, NULL);
				}

				//use for debugging in console
				if (fReplay)
				{
					if (fUseTTS)
						cpVoice->Speak(L"when you said", SPF_ASYNC, NULL);
					else
						printf("\twhen you said...\n");
					cpResult->SpeakAudio(NULL, 0, NULL, NULL);
				}

				cpResult.Release();
			}

			cpGrammar->SetDictationState(SPRS_ACTIVE);
			return dstrText;
		}
	}
}

//not used only for testing function
bool Listen::hear()
{
	//try and hear something
	heard = hearing();

	//if input is heard
	if (heard.Length() > 0) {
		return true;
	}
	else {
		return false;
	}

}

CSpDynamicString Listen::getheard()
{
	return heard;
}

CSpDynamicString Listen::hearing() {
	HRESULT hr = S_OK;
	CComPtr<ISpRecognizer> cpRecoEngine;
	CComPtr<ISpRecoContext> cpRecoCtxt;
	CComPtr<ISpRecoGrammar> cpGrammar;
	CComPtr<ISpVoice> cpVoice;

	hr = cpRecoEngine.CoCreateInstance(CLSID_SpInprocRecognizer);
	if (SUCCEEDED(hr))
	{
		hr = cpRecoEngine->CreateRecoContext(&cpRecoCtxt);
	}
	if (SUCCEEDED(hr))
	{
		hr = cpRecoCtxt->GetVoice(&cpVoice);
	}
	// create default audio object
	CComPtr<ISpAudio> cpAudio;
	hr = SpCreateDefaultObjectFromCategoryId(SPCAT_AUDIOIN, &cpAudio);

	// set the input for the engine
	hr = cpRecoEngine->SetInput(cpAudio, TRUE);
	hr = cpRecoEngine->SetRecoState(SPRST_ACTIVE);

	if (cpRecoCtxt && cpVoice &&
		SUCCEEDED(hr = cpRecoCtxt->SetNotifyWin32Event()) &&
		SUCCEEDED(hr = cpRecoCtxt->SetInterest(SPFEI(SPEI_RECOGNITION), SPFEI(SPEI_RECOGNITION))) &&
		SUCCEEDED(hr = cpRecoCtxt->SetAudioOptions(SPAO_RETAIN_AUDIO, NULL, NULL)) &&
		SUCCEEDED(hr = cpRecoCtxt->CreateGrammar(0, &cpGrammar)) &&
		SUCCEEDED(hr = cpGrammar->LoadDictation(NULL, SPLO_STATIC)) &&
		SUCCEEDED(hr = cpGrammar->SetDictationState(SPRS_ACTIVE)))
	{

		// Load the appropriate grammars
		hr = LoadGrammars(cpGrammar, cpRecoCtxt, cpRecoEngine);
		if (FAILED(hr))
		{
			HRESULT hr2 = ERROR_RESOURCE_LANG_NOT_FOUND;
			if ((SPERR_UNSUPPORTED_LANG == hr) || (ERROR_RESOURCE_LANG_NOT_FOUND == (0xffff & hr)))
			{
				printf("unsuppoted lang\r\n");
			}

			return hr;
		}

		USES_CONVERSION;
		CSpEvent event;
		CComPtr<ISpRecoResult> cpResult;

		while (SUCCEEDED(hr = BlockForResult(cpRecoCtxt, &cpResult)))
		{
			CSpDynamicString dstrText;
			if (SUCCEEDED(cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE,
				TRUE, &dstrText, NULL)))
			{
				if (debug)
					printf("I heard:  %s\n", W2A(dstrText));

				if (fUseTTS)
				{
					cpVoice->Speak(L"I heard", SPF_ASYNC, NULL);
					cpVoice->Speak(dstrText, SPF_ASYNC, NULL);
				}

				//use for debugging in console
				if (fReplay)
				{
					if (fUseTTS)
						cpVoice->Speak(L"when you said", SPF_ASYNC, NULL);
					else
						printf("\twhen you said...\n");
					cpResult->SpeakAudio(NULL, 0, NULL, NULL);
				}
				cpResult.Release();
			}

			cpGrammar->SetDictationState(SPRS_ACTIVE);
			return dstrText;
		}
	}

}

HRESULT Listen::LoadGrammars(CComPtr<ISpRecoGrammar> m_cpDictGrammar, CComPtr<ISpRecoContext> m_cpDictRecoCtxt, CComPtr<ISpRecognizer> m_cpRecoEngine)
{
	// Create the grammar for general dictation, and make it the statistical
	// language model for dictation
	m_cpDictGrammar.Release();
	HRESULT hr = m_cpDictRecoCtxt->CreateGrammar(GID_DICTATION, &m_cpDictGrammar);
	if (SUCCEEDED(hr))
	{
		hr = m_cpDictGrammar->LoadDictation(NULL, SPLO_STATIC);
	}
	if (FAILED(hr))
	{
		m_cpDictGrammar.Release();
	}

	// We need a langid from the engine in order to load the grammars in the correct language
	SPRECOGNIZERSTATUS Stat;
	LANGID langid = 0;
	::memset(&Stat, 0, sizeof(Stat));
	if (SUCCEEDED(hr))
	{
		hr = m_cpRecoEngine->GetStatus(&Stat);
	}
	if (SUCCEEDED(hr))
	{
		langid = Stat.aLangID[0];
	}

	return hr;
}

//using the word it's for testing against single quotes for the sql query
//if more letters are required to be removed then can update this function to pass in the find string to loop through
std::string Listen::clean_string(std::string str)
{
	std::string find = "\'";
	std::string result = str;
	int pos = 0;
	pos = str.find(find);
	while (pos != std::string::npos) {
		int length = result.length();

		if (pos != -1)
			result.erase(pos, 1);
		pos = result.find(find);
	}
	return result;

}

