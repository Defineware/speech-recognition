/*
Usage:
	Should have running on it's own thread as function will put a hold while waiting on a command.

Usage Call:
	#include "listen.h" // in file that will call the below
	Listen listen;//listen object

	//continue to listen
	do {
		CSpDynamicString str = "";
		str = listen.hearing();
		listen.hearing();
	} while (listen.getheard() <= 0);
*/

#ifndef LISTEN_H
#define LISTEN_H

#define WM_RECOEVENT    WM_APP      // Window message used for recognition events
#define GID_DICTATION   0           // Dictation grammar has grammar ID 0


#include <windows.h>
#include <sapi.h>
#include <stdio.h>
#include <string.h>
#include <atlbase.h>
#include "sphelper.h"
#include <string>

class Listen {
public:
	Listen() {}
	~Listen() { ::CoUninitialize(); }
	inline HRESULT BlockForResult(ISpRecoContext * pRecoCtxt, ISpRecoResult ** ppResult);
	CSpDynamicString hearing();//private SR engine
	CSpDynamicString hearing_shared();//windows command shared SR engine
	bool hear();
	CSpDynamicString getheard();
	HRESULT LoadGrammars(CComPtr<ISpRecoGrammar> cpGrammar, CComPtr<ISpRecoContext> cpRecoCtxt, CComPtr<ISpRecognizer> cpRecoEngine);
	std::string clean_string(std::string str);
private:
	HRESULT hr = E_FAIL;
	CSpEvent event;
	bool fUseTTS = false;           // turn TTS play back on or off
	bool fReplay = false;           // turn Audio replay on or off
	bool debug = true;				// turn on other debugging messages in console
	CSpDynamicString heard;			// what has been heard
};
#endif

